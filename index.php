<?php
class minecraft_skins {

    public function common($params) {
        $mes = '';
        $html = '';

        $patch = 'plugins/minecraft_skins/';

        $url = 'http://'.$_SERVER['HTTP_HOST'].'/';

        $username = $_SESSION['user']['name'];

        if ($username == ''){
            $html = "Сначала авторизуйтесь!";
        } else {
            if(isset($_POST['skincharge']) && is_uploaded_file($_FILES["filename"]["tmp_name"]))
            {
                $imageinfo = getimagesize($_FILES['filename']['tmp_name']);

                if ($_FILES['filename']['type'] = "image/png" && $imageinfo['mime'] = 'image/png' &&
                    $imageinfo["0"] = '64' && $imageinfo["1"] = '32' &&
                    preg_match("/\.(png)$/i", $_FILES['filename']['name'])) {

                    move_uploaded_file($_FILES["filename"]["tmp_name"], R.$patch.'skins/'.$username.'.png');
                    $mes = "Скин успешно обновлен<br>";
                }else{
                    $mes = "Ошибка загрузки файла!<br>";
                }
            }

            if(isset($_POST['deleteskin'])){
                unlink(R.$patch.'skins/'.$username.'.png');
                $mes = "Скин успешно удален<br>";
            }

            $html .= $mes;

            $html .= "Это твой скин";
            $html .= '<br>';
            if (file_exists($patch.$username.'.png'))
                $skinpath = $url.$patch.'skins/'.$username.'.png';
            else
                $skinpath = $url.$patch.'skins/'.'default.png';

            $html .= '<img src="'.$url.$patch.'inc/skin2d.php?skinpath='.$skinpath.'" />';
            $html .= '<br>';
            $html .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="POST" enctype="multipart/form-data" style="margin-bottom: 0; padding-bottom: 1em">Файл .png размером 64x32:<br>';
            $html .= '<input type="file" name="filename"><br>';
            $html .= '<input type="submit" name="skincharge" value="Загрузить">';
            $html .= '<input type="submit" name="deleteskin" value="Удалить скин">';
            $html .= '</form>';
        }
        $params = str_replace('{{ minecraft_skins }}', $html, $params);




        $mes = '';
        $html = '';
        if ($username == '') {
            $html = "Сначала авторизуйтесь!";
        } else {
            if (isset($_POST['cloakcharge']) && is_uploaded_file($_FILES["filename"]["tmp_name"])) {
                $ImageSize = getimagesize($_FILES['filename']['tmp_name']);
                if ((($ImageSize['0'] == 22 && $ImageSize['1'] == 17) or
                    ($ImageSize['0'] == 64 && $ImageSize['1'] == 32)) &&
                    $_FILES['filename']['type'] = "image/png" && $imageinfo['mime'] = 'image/png' &&
                    preg_match("/\.(png)$/i", $_FILES['filename']['name'])) {
                        move_uploaded_file($_FILES["filename"]["tmp_name"], R.$patch.'cloaks/'.'/'.$username.'.png');
                        $mes = "Плащ успешно обновлен<br>";
                } else {
                    $mes = "Ошибка загрузки файла!<br>";
                }
            }

            if (isset($_POST['deletecloak'])) {
                unlink(R.$patch.'cloaks/'.$username.'.png');
                $mes = "Плащ успешно удален<br>";
            }
            $html .= '<font color="red">';
            $html .= $mes;
            $html .= '</font>';

            if (!file_exists(R.$patch.'cloaks/'.$username.'.png')) {
                $html .= 'Плащ не установлен<br>';
            } else {
                $html .= 'Это твой плащ<br><img src="'.$url.$patch.'cloaks/'.$username.'.png"/>';
            }
                    
            $html .= '<br>
            <form action="'.$_SERVER['REQUEST_URI'].'" method="POST" enctype="multipart/form-data">Файл .png размером 22x17 или 64x32:<br>
             <input type="file" name="filename"><br>
             <input type="submit" name="cloakcharge" value="Загрузить"><input type="submit" value="Удалить плащ" name="deletecloak">
            </form>';
        }

        return str_replace('{{ minecraft_cloaks }}', $html, $params);
    }

}
?>